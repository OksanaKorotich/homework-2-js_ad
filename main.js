'use strict';

const books = [
    {
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70
    },
    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    },
    {
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    },
    {
     author: "Террі Пратчетт",
     name: "Рухомі картинки",
     price: 40
    },
    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
    }
  ];


  let booksDir = document.getElementById('root');
  console.log(booksDir);

    function createBooksList(books){
        let booksList = document.createElement('ul');

        books.forEach(book => {
            try{
                if(!book.hasOwnProperty('author')){
                throw new Error(`Book "${book.name}" haven't info about author`);
                }
                if(!book.hasOwnProperty('name')){
                    throw new Error(`Book by "${book.author}" haven't info about name`);
                }
                if(!book.hasOwnProperty('price')){
                    throw new Error(`Book "${book.name}" haven't info about price`);
                }

                let booksListItem = document.createElement('li');
                let bookName = document.createElement('h3');
                let bookAuthor = document.createElement('p');
                let bookPrice = document.createElement('p');

                bookName.innerText = book.name;
                bookAuthor.innerText = `Author: ${book.author}`;
                bookPrice.innerText = `Price: ${book.price}$`;

                booksListItem.appendChild(bookName);
                booksListItem.appendChild(bookAuthor);
                booksListItem.appendChild(bookPrice);
                booksList.appendChild(booksListItem);
            }
            catch (e) {
              console.error(e.message);
            }
        });

        return booksList;

    }

    let booksList = createBooksList(books);
    booksDir.appendChild(booksList);

